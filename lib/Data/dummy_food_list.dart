import 'package:ecommerce_ui/Models/food_model.dart';

List<FoodItem> foodList = [
  FoodItem(
      name: "Tacos Nanchas Choco",
      price: 120,
      weight: 130,
      cal: 460,
      protein: 30,
      rating: 4,
      item:
          "#Chicken #Juicy BBQ #Vegetables #Potato Wedges #Coleslaw Salad #Healthy Yolk #Spicy Fries #Mushroom",
      imgPath: "1.png",
      inStock: 6),
  FoodItem(
      name: "Porata Masala Spicy",
      price: 100,
      weight: 120,
      cal: 300,
      protein: 45,
      rating: 5,
      item:
          "#Chicken #Juicy BBQ #Vegetables #Potato Wedges #Coleslaw Salad #Healthy Yolk #Spicy Fries #Mushroom",
      imgPath: "2.png",
      inStock: 9),
  FoodItem(
      name: "Taco Kebab with lemon",
      price: 90,
      weight: 100,
      cal: 320,
      protein: 150,
      rating: 3,
      item:
          "#Chicken #Juicy BBQ #Vegetables #Potato Wedges #Coleslaw Salad #Healthy Yolk #Spicy Fries #Mushroom",
      imgPath: "3.png",
      inStock: 2),
  FoodItem(
    name: "Chicken Salad healthy diet",
    price: 50,
    weight: 30,
    cal: 65,
    protein: 50,
    rating: 4,
    item:
        "#Chicken #Juicy BBQ #Vegetables #Potato Wedges #Coleslaw Salad #Healthy Yolk #Spicy Fries #Mushroom",
    imgPath: "4.png",
    inStock: 3,
  ),
  FoodItem(
      name: "Chicken Eggs and sauces",
      price: 20,
      weight: 30,
      cal: 120,
      rating: 5,
      protein: 310,
      item:
          "#Chicken #Juicy BBQ #Vegetables #Potato Wedges #Coleslaw Salad #Healthy Yolk #Spicy Fries #Mushroom",
      imgPath: "5.png",
      inStock: 8),
];

List<String> categoriesList = [
  "Pizza",
  "Tacos",
  "Burger",
];
