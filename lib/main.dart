import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Pages/CartPage/Controllers/cart_page_controller.dart';
import 'package:ecommerce_ui/Pages/Settings/Controllers/settings_page_controllers.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Pages/HomePage/home_page.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ListenableProvider<CartPageController>(create: (_) => CartPageController()),
        ListenableProvider<SettingsPageControllers>(create: (_) => SettingsPageControllers()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FOOD Shop',
      debugShowCheckedModeBanner: false,
      themeMode: Provider.of<SettingsPageControllers>(context).isDark ? ThemeMode.dark : ThemeMode.light,
      theme: ThemeData(
        primaryColor: AppColors.greenColor,
        primaryColorLight: AppColors.greenLightColor,
        accentColor: AppColors.grayColor,
        errorColor: AppColors.redColor,
        highlightColor: AppColors.darkColor,
      ),
      darkTheme: ThemeData(
        primaryColor: AppColors.greenColor,
        primaryColorLight: AppColors.greenLightColor,
        accentColor: AppColors.grayColor,
        errorColor: AppColors.redColor,
        highlightColor: AppColors.darkColor,
      ),
      home: HomePage(),
    );
  }
}
