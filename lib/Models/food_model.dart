class FoodItem {
  final String name;
  final double price;
  final double weight;
  final double cal;
  final double protein;
  final String item;
  final String imgPath;
  final int rating;
  final int inStock;

  FoodItem(
      {this.name,
        this.price,
        this.weight,
        this.cal,
        this.protein,
        this.item,
        this.rating,
        this.inStock,
        this.imgPath});
}