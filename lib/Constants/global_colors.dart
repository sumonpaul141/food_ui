import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class AppColors {
  static final Color greenColor = Color(0XFFc4de8c);
  static final Color greenLightColor = Color(0XFFe1edc8);
  static final Color redColor = Color(0XFFcc3e12);
  static final Color darkColor = Color(0XFF131313);
  static final Color grayColor = Color(0XFFEFF5E7);
}