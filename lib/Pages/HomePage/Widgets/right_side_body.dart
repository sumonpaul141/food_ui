import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Data/dummy_food_list.dart';
import 'package:ecommerce_ui/Pages/HomePage/Widgets/food_item_card.dart';
import 'package:ecommerce_ui/Pages/HomePage/Widgets/food_item_horizonatal.dart';
import 'package:ecommerce_ui/Pages/HomePage/Widgets/home_app_bar.dart';
import 'package:flutter/material.dart';

class RightSideBody extends StatelessWidget {
  const RightSideBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 60),
      child: SingleChildScrollView(
        child: Column(
          children: [
            HomeAppBar(),
            Container(
              height: 350,
              child: ListView.builder(
                  itemCount: foodList.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, i) {
                    double pad = i == 0 ? 20.0 : 0.0;
                    return Padding(
                      padding: EdgeInsets.only(left: pad),
                      child: FoodItemCard(
                        foodItem: foodList[i],
                      ),
                    );
                  }),
            ),
            Container(
              padding: EdgeInsets.only(left: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Popular",
                    style: TextStyle(
                        color: AppColors.darkColor,
                        fontSize: 26,
                        fontWeight: FontWeight.w800),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ListView.builder(
                      itemCount: foodList.length,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, i) {
                        return FoodItemHorizontal(
                          foodItem: foodList[i],
                        );
                      }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
