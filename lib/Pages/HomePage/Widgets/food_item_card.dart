import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Models/food_model.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/details_page.dart';
import 'package:flutter/material.dart';
class FoodItemCard extends StatelessWidget {
  final FoodItem foodItem;

  const FoodItemCard({Key key, this.foodItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (b) => DetailsPage(item: foodItem,)));
      },
      child: Container(
        height: 350,
        width: 220,
        padding: EdgeInsets.only(right: 20),
        child: Stack(
          children: [
            Positioned(
              top: 70,
              left: 0,
              child: Container(
                height: 250,
                width: 180,
                decoration: BoxDecoration(
                    color: AppColors.greenColor,
                    borderRadius: BorderRadius.circular(20.0)),
              ),
            ),
            Positioned(
                right: 0,
                top: 0,
                child: Image.asset(
                  "assets/"+foodItem.imgPath,
                  height: 150,
                  width: 150,
                )),
            Positioned(
              top: 180,
              left: 10,
              child: Container(
                width: 160,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          height: 20,
                          child: ListView.builder(
                              itemCount: foodItem.rating,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                return Icon(Icons.star, size: 15, color: AppColors.darkColor,);
                              }),
                        ),
                        Text(" (${foodItem.rating})", style: TextStyle( color: AppColors.darkColor),)
                      ],
                    ),
                    Text(foodItem.name, style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: AppColors.darkColor
                    ),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,)
                  ],
                ),
              ),
            ),
            Positioned(
                right: 10,
                bottom: 20,
                child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: 25, vertical: 15),
                  decoration: BoxDecoration(
                      color: AppColors.redColor,
                      borderRadius: BorderRadius.circular(20)),
                  child: Text(
                    foodItem.price.toString() + " Tk.",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
