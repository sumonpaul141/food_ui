import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Models/food_model.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/details_page.dart';
import 'package:flutter/material.dart';
class FoodItemHorizontal extends StatelessWidget {
  final FoodItem foodItem;
  const FoodItemHorizontal({Key key, this.foodItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (b) => DetailsPage(item: foodItem,)));
      },
      child: Container(
        height: 150,
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          color: AppColors.grayColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20)),
        ),
        child: Row(
          children: [
            SizedBox(
              width: 20,
            ),
            Hero(
              tag: "foodImage" + foodItem.name,
              transitionOnUserGestures: true,
              child: Image.asset(
                "assets/" + foodItem.imgPath,
                height: 120,
                width: 120,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      foodItem.name,
                      style: TextStyle(
                        color: AppColors.darkColor,
                        fontSize: 18,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    RichText(
                      text: TextSpan(
                          text: "${foodItem.price} tk ",
                          style: TextStyle(
                            color: AppColors.redColor,
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                          children: [
                            TextSpan(
                              text: " ( ${foodItem.cal} cal servings ) ",
                              style: TextStyle(
                                color: AppColors.darkColor,
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ]),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
