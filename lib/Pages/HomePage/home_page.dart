import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Data/dummy_food_list.dart';
import 'package:ecommerce_ui/Pages/HomePage/Widgets/food_item_card.dart';
import 'package:ecommerce_ui/Pages/HomePage/Widgets/food_item_horizonatal.dart';
import 'package:ecommerce_ui/Pages/HomePage/Widgets/home_app_bar.dart';
import 'package:ecommerce_ui/Pages/HomePage/Widgets/right_side_body.dart';
import 'package:ecommerce_ui/Pages/Settings/settings_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.greenLightColor,
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              Container(
                width: 60,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(color: AppColors.greenColor),
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (b) => SettingsPage()));
                      },
                      child: Icon(
                        Icons.settings,
                        size: 50,
                        color: AppColors.grayColor,
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        color: AppColors.grayColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Icon(Icons.menu),
                    )
                  ],
                ),
              ),
              RightSideBody(),
              Positioned(
                bottom: 20,
                child: Transform.rotate(
                  angle: -math.pi / 2,
                  alignment: Alignment.topLeft,
                  child: Container(
                    height: 60,
                    child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: categoriesList.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (c, i) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                selectedIndex = i;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: i == selectedIndex
                                    ? AppColors.greenLightColor
                                    : null,
                              ),
                              child: Container(
                                width: 150,
                                child: Center(
                                  child: Text(
                                    categoriesList[i],
                                    style:  TextStyle(
                                            color: i == selectedIndex
                                                ? AppColors.redColor : AppColors.greenLightColor,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 20),
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
