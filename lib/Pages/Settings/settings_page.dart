import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Pages/Settings/Controllers/settings_page_controllers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.greenLightColor,
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_rounded,
            color: AppColors.darkColor,
          ),
        ),
        elevation: 0.5,
        backgroundColor: AppColors.greenLightColor,
        title: Text(
          "Settings",
          style: TextStyle(color: AppColors.darkColor),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Dark Mode",
                  style: TextStyle(
                    color: AppColors.darkColor,
                    fontWeight: FontWeight.w200,
                    fontSize: 20,
                  ),
                ),
                Consumer<SettingsPageControllers>(builder: (context, controller, child){
                  return Switch(
                      value: controller.isDark,
                      onChanged: (val) {
                        print(val);
                        controller.toggleDarkMode();
                      });
                })
              ],
            )
          ],
        ),
      ),
    );
  }
}
