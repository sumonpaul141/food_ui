import 'package:flutter/material.dart';

class SettingsPageControllers with ChangeNotifier{
  bool _isDark = false;

  get isDark => _isDark;

  toggleDarkMode(){
    _isDark = !_isDark;
    notifyListeners();
  }
}