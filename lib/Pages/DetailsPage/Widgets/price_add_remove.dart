import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:flutter/material.dart';

class PriceAddRemove extends StatefulWidget {
  final double price;
  final int inStock;

  const PriceAddRemove({Key key, @required this.price, @required this.inStock})
      : super(key: key);

  @override
  _PriceAddRemoveState createState() => _PriceAddRemoveState();
}

class _PriceAddRemoveState extends State<PriceAddRemove> {
  int itemCount = 1;
  double totalPrice = 0;
  var errorText = "";

  increment() {
    if (itemCount < widget.inStock) {
      setState(() {
        errorText = "";
        itemCount++;
        totalPrice = widget.price * itemCount;
      });
    }else{
      setState(() {
        errorText = "Stock empty";
      });
    }
  }

  decrement() {
    if (itemCount > 1) {
      setState(() {
        errorText = "";
        itemCount--;
        totalPrice = widget.price * itemCount;
      });
    }else{
      setState(() {
        errorText = "Must order one";
      });
    }
  }

  @override
  void initState() {
    totalPrice = widget.price * itemCount;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          totalPrice.toString() + " Tk.",
          style: TextStyle(
            color: AppColors.redColor,
            fontSize: 26,
            fontWeight: FontWeight.bold,
          ),
        ),
        Spacer(
          flex: 1,
        ),
        Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: AppColors.greenColor,
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  InkWell(
                    onTap: decrement,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 20),
                      child: Icon(Icons.remove),
                    ),
                  ),
                  Text(
                    itemCount.toString(),
                    style: TextStyle(
                      color: AppColors.darkColor,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  InkWell(
                    onTap: increment,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 6.0, horizontal: 16),
                      child: Icon(Icons.add),
                    ),
                  ),
                ],
              ),
            ),
            if (errorText != "")
              Text(
                errorText,
                style: TextStyle(color: AppColors.redColor),
              )
          ],
        ),
        Spacer(
          flex: 1,
        ),
      ],
    );
  }
}
