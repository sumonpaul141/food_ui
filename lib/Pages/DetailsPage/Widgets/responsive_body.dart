import 'package:ecommerce_ui/Models/food_model.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/details_appbar.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/details_container.dart';
import 'package:flutter/material.dart';

class ResponsiveBody extends StatelessWidget {
  final bool isPortrait;
  final FoodItem item;

  const ResponsiveBody({Key key, this.isPortrait, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
          Container(
            width: isPortrait
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width * .45,
            child: DetailsAppBar(),
          ),
          Positioned(
            top: isPortrait ? 50 : 0,
            left: isPortrait ? 0 : 70,
            right: isPortrait ? 0 : null,
            bottom: isPortrait ? null : 0,
            child: Hero(
              tag: "foodImage" + item.name,
              transitionOnUserGestures: true,
              child: Image.asset(
                "assets/" + item.imgPath,
                height: 250,
                width: 250,
              ),
            ),
          ),
          Positioned(
            bottom: isPortrait ? 0 : null,
            left: isPortrait ? 0 : null,
            right: 0,
            child: DetailsContainer(
              item: item,
              isPortrait: isPortrait,
            ),
          )
        ],
      ),
    );
  }
}
