import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Models/food_model.dart';
import 'package:ecommerce_ui/Pages/CartPage/Controllers/cart_page_controller.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/price_add_remove.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/top_bottom_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailsContainer extends StatelessWidget {
  final bool isPortrait;
  final FoodItem item;

  const DetailsContainer({Key key, this.isPortrait, this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: isPortrait
          ? MediaQuery.of(context).size.height * 0.55
          : MediaQuery.of(context).size.height - 20,
      width: isPortrait
          ? MediaQuery.of(context).size.width
          : MediaQuery.of(context).size.width * 0.55,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: isPortrait
              ? BorderRadius.only(
              topLeft: Radius.circular(40), topRight: Radius.circular(40))
              : BorderRadius.only(
              topLeft: Radius.circular(40),
              bottomLeft: Radius.circular(40))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            item.name,
            style: TextStyle(
              color: AppColors.darkColor,
              fontSize: 30,
              fontWeight: FontWeight.w400,
            ),
            overflow: TextOverflow.ellipsis,
          ),
          Spacer(
            flex: 1,
          ),
          PriceAddRemove(
            price: item.price,
            inStock: item.inStock,
          ),
          Spacer(
            flex: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TopBottomText(
                topText: "Weight",
                bottomText: item.weight.toString(),
              ),
              TopBottomText(
                topText: "Calories",
                bottomText: item.cal.toString(),
              ),
              TopBottomText(
                topText: "Protein",
                bottomText: item.protein.toString(),
              ),
            ],
          ),
          Spacer(
            flex: 1,
          ),
          TopBottomText(
            topText: "Items",
            bottomText: item.item,
            color: AppColors.darkColor,
          ),
          Spacer(
            flex: 1,
          ),
          Consumer<CartPageController>(builder: (context, cart, child){
            return GestureDetector(
              onTap: () {
                cart.addProductToCart(item);
                SnackBar snackBar = SnackBar(
                  backgroundColor: AppColors.greenColor,
                  padding: EdgeInsets.all(25),
                  content: Text(
                    "Item added to the cart",
                    style: TextStyle(color: AppColors.darkColor, fontSize: 16),
                  ),
                  duration: Duration(milliseconds: 500),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
              child: Container(
                width: double.infinity,
                height: 70,
                decoration: BoxDecoration(
                    color: AppColors.greenColor,
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Text(
                    "Add to Cart",
                    style: TextStyle(
                        color: AppColors.darkColor,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            );
          })
        ],
      ),
    );
  }
}
