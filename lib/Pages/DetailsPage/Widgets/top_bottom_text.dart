import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:flutter/material.dart';

class TopBottomText extends StatelessWidget {
  final String topText, bottomText;
  final Color color;
  const TopBottomText({Key key, this.topText, this.bottomText, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          topText,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
              color: AppColors.darkColor
          ),
        ),
        Text(
          bottomText,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              color: color ?? AppColors.redColor
          ),
        ),
      ],
    );
  }
}
