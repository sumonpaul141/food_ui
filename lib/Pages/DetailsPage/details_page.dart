import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Models/food_model.dart';
import 'package:ecommerce_ui/Pages/CartPage/Controllers/cart_page_controller.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/details_appbar.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/price_add_remove.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/responsive_body.dart';
import 'package:ecommerce_ui/Pages/DetailsPage/Widgets/top_bottom_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailsPage extends StatelessWidget {
  final FoodItem item;

  const DetailsPage({
    Key key,
    this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    return Scaffold(
      backgroundColor: AppColors.greenColor,
      body: ResponsiveBody(
        isPortrait: isPortrait,
        item: item,
      ),
    );
  }
}
