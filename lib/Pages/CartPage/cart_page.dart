import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Data/dummy_food_list.dart';
import 'package:ecommerce_ui/Pages/CartPage/Controllers/cart_page_controller.dart';
import 'package:ecommerce_ui/Pages/CartPage/Widgets/cart_item.dart';
import 'package:ecommerce_ui/Pages/CartPage/Widgets/right_left_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.greenLightColor,
      appBar: AppBar(
        backgroundColor: AppColors.greenLightColor,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios_outlined,
            color: AppColors.darkColor,
          ),
        ),
        title: Text(
          "My Cart",
          style: TextStyle(color: AppColors.darkColor),
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                child: ListView.builder(
                    itemCount: context.watch<CartPageController>().cartItems.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (c, i) {
                      return CartItem(
                        item: context.watch<CartPageController>().cartItems[i],
                      );
                    }),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: AppColors.grayColor,
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                        child: TextField(
                      decoration: InputDecoration(
                          border: InputBorder.none, hintText: "Promo code"),
                    )),
                    Container(
                      height: 50,
                      width: 130,
                      decoration: BoxDecoration(
                        color: AppColors.redColor,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Center(
                        child: Text(
                          "Apply",
                          style: TextStyle(color: AppColors.grayColor),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    width: MediaQuery.of(context).size.width - 20,
                    margin: EdgeInsets.only(bottom: 10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: AppColors.grayColor,
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomRight: Radius.circular(20))),
                    child: Column(
                      children: [
                        RightLeftText(rightText: "Total", leftText: "${context.watch<CartPageController>().totalPrice.toString()} TK"),
                        RightLeftText(
                            rightText: "Shipping", leftText: "+ ${context.watch<CartPageController>().shipping.toString()} TK"),
                        RightLeftText(
                            rightText: "Discount", leftText: "- ${context.watch<CartPageController>().discount.toString()} TK"),
                        Divider(),
                        RightLeftText(
                          rightText: "Payable",
                          leftText: "${context.watch<CartPageController>().payable.toString()} TK",
                          textColorRight: AppColors.redColor,
                          textColorLeft: AppColors.redColor,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: TextButton(
        onPressed: () {
          SnackBar snackBar = SnackBar(
            content: Text(
              "Order placed, Thank you..",
              style: TextStyle(color: AppColors.darkColor),
            ),
            backgroundColor: AppColors.grayColor,
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        style: ButtonStyle(
          padding:
              MaterialStateProperty.resolveWith((states) => EdgeInsets.zero),
        ),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            color: AppColors.redColor,
            borderRadius: BorderRadius.circular(50),
          ),
          height: 50,
          child: Center(
            child: Text(
              "Place order",
              style: TextStyle(color: AppColors.grayColor),
            ),
          ),
        ),
      ),
    );
  }
}
