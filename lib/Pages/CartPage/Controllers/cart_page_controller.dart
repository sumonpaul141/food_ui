import 'package:ecommerce_ui/Models/food_model.dart';
import 'package:flutter/cupertino.dart';

class CartPageController with ChangeNotifier{
  List<FoodItem> cartItems = [];
  double totalPrice = 0.0;
  double shipping = 50.0;
  double discount = 0.0;
  double payable = 0.0;

  addProductToCart(FoodItem foodItem){
    cartItems.add(foodItem);
    countTotalPrice();
  }

  removeProductFromCart(FoodItem foodItem){
    cartItems.remove(foodItem);
    countTotalPrice();
  }

  countTotalPrice(){
    totalPrice = 0.0;
    cartItems.forEach((element) {
      totalPrice = totalPrice + element.price;
    });
    payable = totalPrice + shipping - discount;
    notifyListeners();
  }
}