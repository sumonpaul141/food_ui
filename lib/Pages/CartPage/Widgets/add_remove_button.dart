import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:flutter/material.dart';
class AddRemoveButton extends StatelessWidget {
  const AddRemoveButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              color: AppColors.greenColor,
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              InkWell(
                onTap: () {},
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 4.0, horizontal: 10),
                  child: Icon(Icons.remove),
                ),
              ),
              Text(
                "1",
                style: TextStyle(
                  color: AppColors.darkColor,
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
              ),
              InkWell(
                onTap: () {},
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 4.0, horizontal: 10),
                  child: Icon(Icons.add),
                ),
              ),
            ],
          ),
        ),
        // if (errorText != "")
        //   Text(
        //     errorText,
        //     style: TextStyle(color: AppColors.redColor),
        //   )
      ],
    );
  }
}
