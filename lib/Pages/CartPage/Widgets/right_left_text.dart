import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:flutter/material.dart';
class RightLeftText extends StatelessWidget {
  final String rightText, leftText;
  final Color textColorRight, textColorLeft;

  const RightLeftText({Key key, @required this.rightText, @required this.leftText, this.textColorRight, this.textColorLeft}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            rightText??"",
            style: TextStyle(
                color: textColorLeft ?? AppColors.darkColor.withOpacity(0.8),
                fontSize: 13,
                fontWeight: FontWeight.normal,
            ),
          ),
          Text(
            leftText??"",
            style: TextStyle(
                color: textColorRight ?? AppColors.darkColor,
                fontSize: 14,
                fontWeight: FontWeight.bold
            ),
          ),
        ],
      ),
    );
  }
}