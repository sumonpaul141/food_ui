import 'package:ecommerce_ui/Constants/global_colors.dart';
import 'package:ecommerce_ui/Models/food_model.dart';
import 'package:ecommerce_ui/Pages/CartPage/Controllers/cart_page_controller.dart';
import 'package:ecommerce_ui/Pages/CartPage/Widgets/add_remove_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartItem extends StatelessWidget {
  final FoodItem item;

  const CartItem({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topRight,
      child: Container(
        width: MediaQuery.of(context).size.width - 20,
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: AppColors.grayColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), bottomLeft: Radius.circular(20))),
        child: Row(
          children: [
            Image.asset(
              "assets/" + item.imgPath,
              height: 100,
              width: 100,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.name ?? "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                        color: AppColors.darkColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                  Text(
                    "${item.price} TK",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                        color: AppColors.redColor,
                        fontWeight: FontWeight.w900,
                        fontSize: 16),
                  ),
                ],
              ),
            ),
            Consumer<CartPageController>(
              builder: (context, controller, child) {
                return GestureDetector(
                  onTap: (){
                    controller.removeProductFromCart(item);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                    ),
                    child: Icon(
                      Icons.delete,
                      color: AppColors.redColor,
                    ),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
